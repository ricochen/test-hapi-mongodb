const Hapi = require('hapi')
const Boom = require('boom')
const HapiMongodb = require('hapi-mongodb')

const goodOpts = {
	ops: {
		interval: 1000
	},
	reporters: {
		myConsoleReporter: [{
			module: 'good-squeeze',
			name: 'Squeeze',
			args: [{ log: '*', response: '*'  }]
		}, {
			module: 'good-console'
		}, 'stdout']
	}
}
const server = new Hapi.Server()
const actions = {
	getUsers: (request, reply) => {
		request.miscDb.db.collection('users').find({}).toArray().then((docs) => {
			reply(docs)
		}).catch((err) => {
			return reply(Boom.serverTimeout(err))
		})
	},
	getProducts: (request, reply) => {
		request.miscDb.db.collection('products').find({}).toArray().then((docs) => {
			reply(docs)
		}).catch((err) => {
			return reply(Boom.serverTimeout(err))
		})
	},
	addUser: (request, reply) => {
		request.miscDb.db.collection('users').insertOne(request.payload).then(result => {
			reply(result)
		}).catch(err => {
			reply(Boom.serverTimeout(err))
		})
	}
}

server.connection({
	host: 'localhost',
	port: 8080,
	routes: {
		log: true
	}
})

const routes = [
	{
		method: 'GET',
		path: '/api/users',
		handler: actions.getUsers
	},
	{
		method: 'GET',
		path: '/api/products',
		handler: actions.getProducts
	},
	{
		method: 'POST',
		path: '/api/users',
		handler: actions.addUser
	}
]

server.route(routes)

const dbOpts = {
	url: 'mongodb://localhost:27017/misc',
	decorate: 'miscDb'
}

server.register([{
	register: HapiMongodb,
	options: dbOpts
}, {
	register: require('good'),
	options: goodOpts
}], (err) => {
	if(err) {
		console.log(err)
		throw err
	}
	server.start(() => {
		console.log(`server started at ${server.info.uri}`)
	})
})

// handles SIGTERM
const handleShutdown = () => {
	console.log('to handle shutdown')
	server.stop()
}
process.on('SIGINT', handleShutdown)
process.on('SIGTERM', handleShutdown)
